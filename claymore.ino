/*
  Arduino Fake Claymore

*/

// Motion detector
const int MOTION_DETECTOR = 2;
// Buzzer
const int BUZZER = 3;
// Button
const int BUTTON = 4;
// DC Motor
const int MOTOR = 5;
// LEDS
const int RED_LED = 7;
const int GREEN_LED = 8;

const int ALARM_TONE_1 = 700;
const int ALARM_TONE_2 = 2600;
const int ACTIVE_TONE = 1330;
const int ERROR_TONE = 100;

int buttonState = 0;

void
setup()
{
  Serial.begin(9600); // set up Serial library at 9600 bps
  Serial.println(" IL DON");

  // PinModes
  pinMode(GREEN_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);
  pinMode(MOTOR, OUTPUT);
  pinMode(BUTTON, INPUT);

  tone(BUZZER, ACTIVE_TONE, 30);
  digitalWrite(GREEN_LED, HIGH);
}

void
loop()
{
  // read the state of the pushbutton value:
  buttonState = digitalRead(BUTTON);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    armed();
  }
}

// Blink the green led for some seconds, with duty cycle of 50%
void
blinkGreenLed(int seconds)
{
  blinkPin(GREEN_LED, seconds);
}

// Blink the red led for some seconds, with duty cycle of 50%
void
blinkRedLed(int seconds)
{
  blinkPin(RED_LED, seconds);
}

// Blink a pin for some seconds, with duty cycle of 50%
void
blinkPin(int pin, int seconds)
{
  for (int second = 0; second < seconds; second++) {
    digitalWrite(pin,
                 HIGH);     // turn the LED on (HIGH is the voltage level)
    delay(500);             // wait for half of a second
    digitalWrite(pin, LOW); // turn the LED off by making the voltage LOW
    delay(500);             // wait for half of a second
  }
}

void
armed()
{
  digitalWrite(GREEN_LED, LOW);
  blinkGreenLed(3);
  blinkRedLed(8);
  digitalWrite(RED_LED, HIGH);
  while (1) {
    tone(BUZZER, ACTIVE_TONE, 30);
    delay(500);
  }
}